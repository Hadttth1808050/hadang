#include <stdio.h>
#include <mem.h>

void concat(char s1[], char s2[]) {
    strcat(s1, s2);
}

void removestdchar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}

int main() {
    char s1[20];
    char s2[20];
    printf("Enter the first string:\n");
    fgets(s1, sizeof(s1) * sizeof(char), stdin);
    removestdchar(s1);
    s1[strlen(s1) - 1] = ' ';
    printf("Enter the second string:\n");
    fgets(s2, sizeof(s2) * sizeof(char), stdin);
    removestdchar(s2);
    concat(s1, s2);
    printf("the concatenated string:%s", s1);

    return 0;
}